package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SuppressWarnings("unused")
@SpringBootApplication
@ComponentScan({"com.example"})
public class TestProject1Application {

	public static void main(String[] args) {
		SpringApplication.run(TestProject1Application.class, args);
	}

}
