package com.example.restcontroller;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.CalculatorService;


@RestController
@Path("/testcontroller")
public class TestController {
	
	@Autowired
    CalculatorService CS;
	
    @GetMapping(value = "/print", produces = MediaType.APPLICATION_JSON_VALUE)
    public String print() {
        String string = "Welcome To The Resistance";
        return string;
    }
    
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(value = "/get/{a}/{b}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addNumber(@PathVariable("a") int a, @PathVariable("b") int b ) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        String s = Integer.toString(CS.add(a, b));
        return new ResponseEntity<String>(s, headers, HttpStatus.OK);
    }
} 
