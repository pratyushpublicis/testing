package com.example.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.service.CalculatorService;

@SuppressWarnings("unused")
public class TestCases {
	
	CalculatorService CS = new CalculatorService();

	@Test
	public void testAdd() {
		int expected = 9;
		assertEquals(expected, CS.add(3, 6));
	}

}
