FROM openjdk:8
ADD target/TestProject1-0.0.1-SNAPSHOT.jar TestProject1-0.0.1-SNAPSHOT.jar
EXPOSE 8099
ENTRYPOINT [ "java", "jar", "TestProject1-0.0.1-SNAPSHOT.jar" ]